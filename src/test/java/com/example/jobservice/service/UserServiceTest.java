package com.example.jobservice.service;

import com.example.jobservice.entity.User;
import com.example.jobservice.repository.UserRepository;
import com.example.jobservice.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class UserServiceTest {
    @Mock
    private UserRepository repository;
    @InjectMocks
    private UserService service;

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void shouldReturnUserList(){
        User user = new User();
        when(repository.findAll()).thenReturn(Collections.singletonList(user));

        List<User> users = service.listAll();

        assertThat(users).containsExactly(user);
    }

    @Test
    public void shouldSaveUser() {
        User userToReturn = new User();
        userToReturn.setName("UserName");
        when(repository.save(any(User.class))).thenReturn(userToReturn);

        User save = service.save(userToReturn);

        assertThat(save.getName()).isEqualTo("UserName");
        verify(repository, times(1)).save(Mockito.any(User.class));
    }

    @Test
    public void shouldDeleteUser() {
        service.delete("User");

        verify(repository, times(1)).deleteById(Mockito.anyString());
    }

    @Test
    public void shouldCheckIfUserExist() {
        service.contains("User");

        verify(repository, times(1)).existsById(Mockito.anyString());
    }

}