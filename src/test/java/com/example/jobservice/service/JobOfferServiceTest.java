package com.example.jobservice.service;

import com.example.jobservice.entity.JobOffer;
import com.example.jobservice.repository.JobOfferRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.mockito.Mockito.when;

public class JobOfferServiceTest {

    @Mock
    private JobOfferRepository repository;
    @InjectMocks
    private JobOfferService service;

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void shouldReturnProperJobOfferList(){
        JobOffer jobOffer1 = createJobOffer("User1", 1);
        JobOffer jobOffer2 = createJobOffer("User2", 1);
        JobOffer jobOffer3 = createJobOffer("User2", 2);
        when(repository.findAll()).thenReturn(Arrays.asList(jobOffer1,jobOffer2, jobOffer3));

        List<JobOffer> jobs = service.list("User2", "1");

        assertThat(jobs).containsExactly(jobOffer2);
    }

    private JobOffer createJobOffer(String username, int category) {
        JobOffer jobOffer = new JobOffer();
        jobOffer.setUsername(username);
        jobOffer.setJobCategory(category);
        return jobOffer;
    }
}