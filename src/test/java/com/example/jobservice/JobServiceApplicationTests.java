package com.example.jobservice;

import com.example.jobservice.controller.JobOfferController;
import com.example.jobservice.controller.UserController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class JobServiceApplicationTests {

	@Autowired
	private UserController userController;

	@Autowired
	private JobOfferController jobOfferController;

	@Test
	void contextLoads() {
		assertThat(userController).isNotNull();
		assertThat(jobOfferController).isNotNull();
	}

}
