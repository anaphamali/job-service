package com.example.jobservice.controller;

import com.example.jobservice.entity.User;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;


public class UserControllerWebServiceTest extends AbstractControllerTest {
    public static final String URI = "/users";

    @Override
    @Before
    public void setUp() {
        super.setUp();
    }

    @Test
    public void shouldCreateUser() throws Exception {
        MvcResult mvcResult = postUser();

        MockHttpServletResponse response = mvcResult.getResponse();

        assertEquals(200, response.getStatus());
        assertThat(response.getContentAsString())
                .contains("user")
                .contains("login")
                .contains("pwd");

        deleteUser();
    }

    @Test
    public void shouldReturnUserList() throws Exception {
        postUser();
        MvcResult mvcResult = getUsers();

        MockHttpServletResponse response = mvcResult.getResponse();
        assertEquals(200, response.getStatus());
        User[] userList = super.mapFromJson(response.getContentAsString(), User[].class);
        assertTrue(userList.length > 0);

        deleteUser();
    }

    @Test
    public void shouldUpdateUser() throws Exception{
        postUser();
        User user = createUser();
        String inputJson = super.mapToJson(user);

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.put(URI+"/user")
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();

        MockHttpServletResponse response = mvcResult.getResponse();
        assertEquals(200, response.getStatus());
        assertThat(response.getContentAsString())
                .contains("user")
                .contains("login")
                .contains("pwd");

        deleteUser();
    }

    @Test
    public void shouldDeleteUser() throws Exception{
        postUser();

        MvcResult mvcResult = deleteUser();

        MockHttpServletResponse response = mvcResult.getResponse();
        assertEquals(200, response.getStatus());
    }

    private MvcResult getUsers() throws Exception {
        return mvc.perform(get(URI)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
    }

    private MvcResult deleteUser() throws Exception {
        return mvc.perform(MockMvcRequestBuilders.delete(URI + "/user")).andReturn();
    }

    private MvcResult postUser() throws Exception {
        User user = createUser();
        String inputJson = super.mapToJson(user);

        return mvc.perform(MockMvcRequestBuilders.post(URI)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
    }

    private User createUser() {
        User user = new User();
        user.setName("user");
        user.setLogin("login");
        user.setPassword("pwd");
        return user;
    }
}