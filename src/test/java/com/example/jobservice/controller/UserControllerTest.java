package com.example.jobservice.controller;

import com.example.jobservice.entity.User;
import com.example.jobservice.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Collections;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class UserControllerTest extends AbstractControllerTest {

    @MockBean
    private UserService service;

    @Before
    public void setUp() {
       super.setUp();
    }

    @Test
    public void shouldReturnProperUserList() throws Exception {
        when(service.listAll()).thenReturn(Collections.singletonList(createUser()));
        this.mvc.perform(get("/users"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json("[{\"name\":\"user\",\"login\":\"login\",\"password\":\"pwd\",\"creationDate\":null}]"));
    }

    private User createUser() {
        User user = new User();
        user.setName("user");
        user.setLogin("login");
        user.setPassword("pwd");
        return user;
    }
}
