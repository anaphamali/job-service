package com.example.jobservice.controller;

import com.example.jobservice.entity.JobOffer;
import com.example.jobservice.service.JobOfferService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Collections;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class JobOfferControllerTest extends AbstractControllerTest {

    @MockBean
    private JobOfferService service;

    @Before
    public void setUp() {
        super.setUp();
    }

    @Test
    public void shouldReturnFilteredJobOfferList() throws Exception {
        when(service.list("user1", "1")).thenReturn(Collections.singletonList(createJobOffer()));
        this.mvc.perform(get("/jobs?username=user1&category=1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json("[{\"idjoboffer\":1,\"username\":\"user1\",\"startDate\":null,\"endDate\":null,\"jobCategory\":1}]"));
    }

    private JobOffer createJobOffer() {
        JobOffer jobOffer = new JobOffer();
        jobOffer.setIdjoboffer(1);
        jobOffer.setUsername("user1");
        jobOffer.setJobCategory(1);
        return jobOffer;
    }


}