package com.example.jobservice.controller;

import com.example.jobservice.entity.User;
import com.example.jobservice.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping(path = "/users")
public class UserController {

    @Autowired
    private UserService service;

    @GetMapping()
    public List<User> list() {
        return service.listAll();
    }

    @GetMapping("/{username}")
    public ResponseEntity<User> find(@PathVariable String username){
        try {
            User user = service.find(username);
            return new ResponseEntity<>(user, HttpStatus.OK);
        } catch (NoSuchElementException e){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping()
    public ResponseEntity<User> create(@RequestBody User user) {
        if (service.contains(user.getName())) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }else {
            User saved = service.save(user);
            return new ResponseEntity<>(saved, HttpStatus.OK);
        }
    }

    @PutMapping("/{username}")
    public ResponseEntity<User> update(@RequestBody User user, @PathVariable String username){
        if (service.contains(username)) {
            service.save(user);
            return new ResponseEntity<>(user, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping(path = "/{username}")
    public ResponseEntity<Object> delete(@PathVariable("username") String username) {
        try {
            service.delete(username);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (ResourceNotFoundException ex) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
