package com.example.jobservice.controller;

import com.example.jobservice.entity.JobOffer;
import com.example.jobservice.service.JobOfferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/jobs")
public class JobOfferController {

    @Autowired
    private JobOfferService service;

    @GetMapping()
    public List<JobOffer> list(@RequestParam(required = false) String username,
                               @RequestParam(required = false) String category) {
        return service.list(username, category);
    }
}
