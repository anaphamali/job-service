package com.example.jobservice.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "joboffer")
public class JobOffer {
    @Id
    @Column(name = "idjoboffer")
    private int idjoboffer;
    @Column(name = "user")
    private String username;
    @Column(name = "startdate")
    private Date startDate;
    @Column(name = "enddate")
    private Date endDate;
    @Column(name = "categoryid")
    private int jobCategory;

    public int getIdjoboffer() {
        return idjoboffer;
    }

    public void setIdjoboffer(int idjoboffer) {
        this.idjoboffer = idjoboffer;
    }

    public int getJobCategory() {
        return jobCategory;
    }

    public void setJobCategory(int jobCategory) {
        this.jobCategory = jobCategory;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
