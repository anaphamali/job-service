package com.example.jobservice.service;

import com.example.jobservice.entity.JobOffer;
import com.example.jobservice.repository.JobOfferRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class JobOfferService {

    @Autowired
    private JobOfferRepository jobRepository;

    public List<JobOffer> list(String name, String category){
        if(name == null && category == null){
            return jobRepository.findAll();
        }
        List<JobOffer> jobOffers = jobRepository.findAll();
        if(name != null && !name.isEmpty()){
            jobOffers = filterByUsername(jobOffers, name);
        }
        if(category != null && !category.isEmpty()){
            jobOffers = filterByCategory(jobOffers, category);
        }
        return jobOffers;
    }

    private List<JobOffer> filterByCategory(List<JobOffer> jobOffers, String category) {
        return jobOffers.stream()
                .filter(j -> String.valueOf(j.getJobCategory()).equals(category))
                .collect(Collectors.toList());
    }

    private List<JobOffer> filterByUsername(List<JobOffer> jobOffers, String name) {
         return jobOffers.stream()
                .filter(j -> j.getUsername().equals(name))
                .collect(Collectors.toList());
    }
}
