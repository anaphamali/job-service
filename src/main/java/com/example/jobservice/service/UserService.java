package com.example.jobservice.service;

import com.example.jobservice.entity.User;
import com.example.jobservice.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;

@Service
@Transactional
public class UserService {
    @Autowired
    private UserRepository userRepository;

    public List<User> listAll() {
        return userRepository.findAll();
    }

    public User find(String name){
        return userRepository.findById(name).get();
    }

    public User save(User user){
        user.setCreationDate(LocalDateTime.now());
        return userRepository.save(user);
    }

    public boolean contains(String id){
        return userRepository.existsById(id);
    }

    public void delete(String name){
        userRepository.deleteById(name);
    }
}
