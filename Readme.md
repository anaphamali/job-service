# CRUD user 
``` 
curl -X GET http://localhost:8080/users
```
``` 
curl -X GET http://localhost:8080/users/username 
```
``` 
curl -X POST -H "Content-Type: application/json" -d "{\"name\":\"username\",\"login\":\"userlogin\", \"password\":\"userpass\"}" http://localhost:8080/users
```
``` 
curl -X PUT -H "Content-Type: application/json" -d "{\"name\":\"username\",\"login\":\"userlogin\", \"password\":\"userpass\"}" http://localhost:8080/users/username
```
```
curl -X DELETE http://localhost:8080/users/ddd
```

# job offer API 
``` 
curl http://localhost:8080/jobs 
```
``` 
curl http://localhost:8080/jobs?username=user&category=1 
```
``` 
curl http://localhost:8080/jobs?username=user
```
``` 
curl http://localhost:8080/jobs?category=2
```

