CREATE TABLE `db`.`users` (
  `name` VARCHAR(45) NOT NULL,
  `login` VARCHAR(45) NULL,
  `password` VARCHAR(45) NULL,
  `creationdate` TIMESTAMP NULL,
  PRIMARY KEY (`name`));
  
CREATE TABLE `db`.`joboffer` (
  `idjoboffer` INT NOT NULL,
  `startdate` TIMESTAMP NULL,
  `enddate` TIMESTAMP NULL,
  `categoryId` INT NULL,
  `user` VARCHAR(45) NULL,
  PRIMARY KEY (`idjoboffer`));
